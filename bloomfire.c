#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef BF_CUSTOM
#define BF_TRACK_COUNT     (1 << 24)
#define BF_RULES_COUNT     (1 << 24)
#define BF_RULES_LIM_COUNT (1 << 24)
#endif

struct bf {
    uint8_t *track[2];
    unsigned track_id;
    uint8_t *rules;
    uint8_t *rules_lim;
    uint64_t key;
};

struct bf_rule {
    uint8_t  ip[4];
    uint16_t port;
    uint8_t  proto;
    uint8_t  pad;
};

struct bf_track {
    struct {
        uint8_t  ip[4];
        uint16_t port;
    } src, dst;
    uint8_t proto;
    uint8_t pad[3];
};

static inline uint64_t
rotl64(uint64_t x, int n)
{
    return (x << n) | (x >> (64 - n));
}

static inline void
sipround(uint64_t v[4])
{
    v[0] += v[1]; v[1] = rotl64(v[1], 13);
    v[1] ^= v[0]; v[0] = rotl64(v[0], 32);
    v[2] += v[3]; v[3] = rotl64(v[3], 16);
    v[3] ^= v[2];
    v[0] += v[3]; v[3] = rotl64(v[3], 21);
    v[3] ^= v[0];
    v[2] += v[1]; v[1] = rotl64(v[1], 17);
    v[1] ^= v[2]; v[2] = rotl64(v[2], 32);
}

static void
bf_hash(uint64_t *h, size_t n, uint64_t mask,
        const void *src, size_t size, const uint64_t k)
{
    uint64_t v[] = {
        UINT64_C(0x736f6d6570736575) ^ (k    ),
        UINT64_C(0x646f72616e646f6d) ^ (k + 8),
        UINT64_C(0x6c7967656e657261) ^ (k    ),
        UINT64_C(0x7465646279746573) ^ (k + 8),
    };

    for (unsigned i = 0; i < size; i += 8) {
        uint64_t m;
        memcpy(&m, &((const uint8_t *)src)[i], 8);
        v[3] ^= m;
        sipround(v);
        v[0] ^= m;
    }
    v[3] ^= (uint64_t)size;
    sipround(v);
    v[0] ^= (uint64_t)size;
    v[2] ^= 0xff;
    sipround(v);
    for (unsigned i = 0; i < n; i++) {
        sipround(v);
        h[i] = (v[0] ^ v[1] ^ v[2] ^ v[3]) & mask;
    }
}

static inline void
bf_hash_track(struct bf *bf, uint64_t h[3], struct bf_track *track)
{
    bf_hash(h, 2, BF_TRACK_COUNT - 1,
            track, sizeof(struct bf_track),
            bf->key);
}

static inline void
bf_hash_rule(struct bf *bf, uint64_t h[3], struct bf_rule *rule)
{
    bf_hash(h, 2, BF_RULES_COUNT - 1,
            rule, sizeof(struct bf_rule),
            bf->key);
}

static inline void
bf_set(uint8_t *map, const uint64_t h)
{
    map[h >> 3] |= (1 << (h & 7));
}

static inline int
bf_get(const uint8_t *map, const uint64_t h)
{
    return map[h >> 3] & (1 << (h & 7));
}

void
bf_create(struct bf *bf, uint64_t key)
{
    bf->track[0] = calloc(1, BF_TRACK_COUNT >> 3);
    bf->track[1] = calloc(1, BF_TRACK_COUNT >> 3);
    bf->track_id = 0;

    bf->rules = calloc(1, BF_RULES_COUNT >> 3);
    bf->rules_lim = calloc(1, BF_RULES_LIM_COUNT);

    bf->key = key;
}

void
bf_add_track(struct bf *bf, struct bf_track *track)
{
    uint64_t h[3];
    bf_hash_track(bf, h, track);
    bf_set(bf->track[0], h[0]);
    bf_set(bf->track[0], h[1]);
    bf_set(bf->track[0], h[2]);
    bf_set(bf->track[1], h[0]);
    bf_set(bf->track[1], h[1]);
    bf_set(bf->track[1], h[2]);
}

void
bf_add_rule(struct bf *bf, struct bf_rule *rule)
{
    uint64_t h[3];
    bf_hash_rule(bf, h, rule);
    bf_set(bf->rules, h[0]);
    bf_set(bf->rules, h[1]);
    bf_set(bf->rules, h[2]);
}

static inline int
bf_check_rule(struct bf *bf, struct bf_rule *rule)
{
    uint64_t h[3];
    bf_hash_rule(bf, h, rule);
    return bf_get(bf->rules, h[0])
        && bf_get(bf->rules, h[1])
        && bf_get(bf->rules, h[2]);
}

static inline int
bf_limit_rule(struct bf *bf, struct bf_rule *rule)
{
    uint64_t h[3];
    bf_hash(h, 3, BF_RULES_LIM_COUNT - 1,
            rule, sizeof(struct bf_rule),
            bf->key);
    int ret = 1;
    for (int i = 0; i < 3; i++) {
        if (bf->rules_lim[h[i]] == 255U)
            continue;
        bf->rules_lim[h[i]]++;
        ret = 0;
    }
    return ret;
}

void
bf_update_rules_lim(struct bf *bf)
{
    memset(bf->rules_lim, 0, BF_RULES_LIM_COUNT);
}

void
bf_update_track(struct bf *bf)
{
    memset(bf->track[bf->track_id], 0, BF_TRACK_COUNT >> 3);
    bf->track_id = 1 - bf->track_id;
}

int
bf_egress(struct bf *bf, struct bf_track *track)
{
    bf_add_track(bf, track);
    return 1;
}

int
bf_ingress(struct bf *bf, struct bf_track *track)
{
    uint64_t h[3];
    bf_hash_track(bf, h, track);
    if (bf_get(bf->track[bf->track_id], h[0]) &&
        bf_get(bf->track[bf->track_id], h[1]) &&
        bf_get(bf->track[bf->track_id], h[2])) {
        bf_set(bf->track[1 - bf->track_id], h[0]);
        bf_set(bf->track[1 - bf->track_id], h[1]);
        bf_set(bf->track[1 - bf->track_id], h[2]);
        return 1;
    }

    _Alignas(8) struct bf_rule rule = {0};
    memcpy(&rule.ip, &track->dst.ip, sizeof(rule.ip));
    memcpy(&rule.port, &track->dst.port, sizeof(rule.port));
    memcpy(&rule.proto, &track->proto, sizeof(rule.proto));

    if (!bf_check_rule(bf, &rule))
        return 0;

    if (bf_limit_rule(bf, &rule))
        return 0;

    bf_set(bf->track[0], h[0]);
    bf_set(bf->track[0], h[1]);
    bf_set(bf->track[0], h[2]);
    bf_set(bf->track[1], h[0]);
    bf_set(bf->track[1], h[1]);
    bf_set(bf->track[1], h[2]);
    return 1;
}
